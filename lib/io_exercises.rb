# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  rand = Random.new
  tries = 0
  guess = rand.rand(99) + 1
  user_guess = nil
  until user_guess == guess
    user_guess = guess_input
    tries += 1
    puts "Guess #{user_guess} was too low" if user_guess < guess
    puts "Guess #{user_guess} was too high" if user_guess > guess
  end
  puts "Congratulation the number was #{guess} it took you #{tries} guesses"
end

def guess_input
  puts "guess a number"
  gets.chomp.to_i
end

def file_shuffler
  file_name = File.new(get_file_name)
  content = file_lines(file_name)
  write_to_file(file_name, content.shuffle)
end

def write_to_file(file_name, content)
  File.open(file_name, "w") do |f|
    content.each { |line| f.puts line }
  end
end

def file_lines(file)
  return [] unless file.is_a? File
  File.readlines(file)
end

def get_file_name
  puts "File name:"
  gets.chomp
end
